<?php

return [
    // Source: https://httpstatuses.com
    '403' => [
        'title'       => '403 - Prohibido',
        'description' => 'El servidor entendió la solicitud pero se niega a autorizarla.',
    ],
    '404' => [
        'title'       => '404 - No encontrado',
        'description' => 'El servidor de origen no encontró una representación actual para el recurso de destino o no está dispuesto a revelar que existe.',
    ],
    '500' => [
        'title'       => '500 - Error interno del servidor',
        'description' => 'El servidor encontró una condición inesperada que le impidió cumplir con la solicitud.',
    ],
    '503' => [
        'title'        => '503 Servicio no Disponible',
        'description'  => 'El servidor actualmente no puede manejar la solicitud debido a una sobrecarga temporal o mantenimiento programado, lo que probablemente se aliviará después de algún retraso.',
    ]
];