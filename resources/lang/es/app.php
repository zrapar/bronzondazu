<?php

return [

    /*
    |--------------------------------------------------------------------------
    | English Application Language Lines
    |--------------------------------------------------------------------------
    */
    'buttons' => [
        'share' => 'Compartir',
        'tweet' => 'Tweet'
    ],
    'footer' => [
        'about'  => 'Acerca de Nosotros',
        'latest' => 'Últimos artículos',
        'share'  => 'Difundir la palabra',
        'text'   => 'Este es solo un simple CMS de Laravel con una interfaz de usuario simple para ayudarlo a acelerar las cosas y concentrarse en la lógica de su proyecto en lugar de recrear la plantilla cada vez.',
        'url'    => 'https://github.com/ozdemirburak/laravel-5-simple-cms'
    ],
    'pagination' => [
        'next'     => 'Siguiente &raquo;',
        'previous' => '&laquo; Anterior',
    ],
    'read_more'    => 'Leer mas'
];