<?php

return [

    /*
    |--------------------------------------------------------------------------
    | English Application App Specific Resources
    |--------------------------------------------------------------------------
    */

    'article' => [
        'create' => 'Crear artículo',
        'edit'   => 'Editar artículo',
        'fields' => [
            'category_id'  => 'Categoría',
            'content'      => 'Contenido',
            'description'  => 'Descripción',
            'published_at' => 'Publicado',
            'title'        => 'Título del artículo',
            'image' => 'Imagen Principal',
            'choose' => 'Selecciona un archivo'
        ],
        'index'  => 'Artículos',
        'show'   => 'Mostrar artículo',
    ],
    'category' => [
        'create' => 'Crear categoría',
        'edit'   => 'Editar categoria',
        'fields' => [
            'article_count' => 'Recuento de artículos',
            'description'   => 'Descripción',
            'title'         => 'Título de categoría',
        ],
        'index'  => 'Categorías',
        'show'   => 'Mostrar categoría',
    ],
    'dashboard' => [
        'fields' => [
            'alexa_local'     => 'Alexa Local',
            'alexa_world'     => 'Alexa World',
            'average_time'    => 'Tiempo promedio',
            'bounce_rate'     => 'Porcentaje de rebote',
            'browsers'        => 'Navegadores',
            'chart_country'   => 'País',
            'chart_region'    => 'Región',
            'chart_visitors'  => 'Visitantes',
            'entrance_pages'  => 'Entrada',
            'exit_pages'      => 'Salida',
            'keywords'        => 'Palabras clave',
            'os'              => 'OS',
            'page_visits'     => 'Visitas a la página',
            'pages'           => 'Páginas',
            'region_visitors' => 'Visitantes de la región',
            'time_pages'      => 'Hora',
            'total_visits'    => 'Visitas totales',
            'traffic_sources' => 'Fuentes de tráfico',
            'unique_visits'   => 'Visitas únicas',
            'visitors'        => 'Visitantes',
            'visits'          => 'Visitas',
            'visits_today'    => 'Visitas hoy',
            'world_visitors'  => 'Distribución mundial de visitantes',
        ],
        'index' => 'Tablero principal'
    ],
    'elfinder' => [
        'index' => 'Administrador de archivos',
    ],
    'page' => [
        'create' => 'Crear página',
        'edit'   => 'Editar página',
        'fields' => [
            'content'      => 'Contenido',
            'description'  => 'Descripción',
            'parent_id'    => 'Padre',
            'title'        => 'Título',
        ],
        'index'  => 'Páginas',
        'show'   => 'Mostrar página',
    ],
    'parent' => [
        'fields' => [
            'title' => 'Página principal ',
        ]
    ],
    'user' => [
        'create' => 'Crear usuario',
        'edit'   => 'Editar usuario',
        'fields' => [
            'email'                 => 'Email',
            'ip_address'            => 'IP',
            'logged_in_at'          => 'Iniciar sesión',
            'logged_out_at'         => 'Cerrar sesión',
            'password'              => 'Contraseña',
            'password_confirmation' => 'Contraseña confirmada',
        ],
        'index'  => 'Usuarios',
        'show'   => 'Mostrar usuario',
    ]
];