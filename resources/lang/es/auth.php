<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Estas credenciales no coinciden con nuestros registros.',
    'login' => [
        'email'    => 'Dirección de correo electrónico',
        'forgot'   => '¿Olvidé mi contraseña?',
        'password' => 'Contraseña',
        'remember' => 'Recuérdame',
        'submit'   => 'Iniciar sesión',
        'title'    => 'Iniciar sesión',
    ],
    'logout'       => 'Cerrar sesión',
    'throttle' => 'Demasiados intentos de acceso. Por favor intente nuevamente en :seconds segundos.',
];