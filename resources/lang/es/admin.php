<?php

$translation = [

    /*
    |--------------------------------------------------------------------------
    | Spanish Language Admin Translations
    |--------------------------------------------------------------------------
    */

    'create' => [
        'fail'          => 'La operación de creación en el recurso ha fallado.',
        'success'       => 'El recurso se ha creado con éxito.'
    ],
    'csrf_error'        => 'Parece que no pudo enviar el formulario durante mucho tiempo. Inténtalo de nuevo.',
    'datatables' => [   // DataTables, files can be found @ https://datatables.net/plug-ins/i18n/
        'sInfo'         => 'Mostrando _START_ de _END_ de _TOTAL_ entradas',
        'sInfoEmpty'    => 'Mostrando 0 de 0 de 0 entradas',
        'sInfoFiltered' => '(filtrando desde _MAX_ entradas totales)',
        'sInfoPostFix'  => '',
        'sLengthMenu'   => 'Mostrar _MENU_ entradas',
        'sProcessing'   => '<div class="overlay"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-refresh-cw"><polyline points="23 4 23 10 17 10"></polyline><polyline points="1 20 1 14 7 14"></polyline><path d="M3.51 9a9 9 0 0 1 14.85-3.36L23 10M1 14l4.64 4.36A9 9 0 0 0 20.49 15"></path></svg></span></div>',
        'sSearch'       => 'Buscar:',
        'sUrl'          => '',
        'sZeroRecords'  => 'No se encontraron registros',
        'oPaginate' => [
            'sFirst'    => 'Primero',
            'sLast'     => 'Final',
            'sNext'     => 'Siguiente',
            'sPrevious' => 'Anterior'
        ]
    ],
    'delete' => [
        'fail'          => 'La operación de eliminación en el recurso ha fallado.',
        'self'          => 'No siempre puedes obtener lo que quieres.',
        'success'       => 'El recurso se ha eliminado con éxito.'
    ],
    'empty'             => 'Todavía no hay registros guardados. ¿Por qué no creas uno nuevo primero?',
    'invalid'           => 'Primero debe configurar su archivo .env para ver el Tablero.',
    'fields' => [
        'created_at'    => 'Creado',
        'deleted_at'    => 'Borrado',
        'no'            => 'No',
        'published_at'  => 'Publicado',
        'reset'         => 'Reiniciar',
        'save'          => 'Salvar',
        'updated_at'    => 'Actualizado',
        'uploaded'      => 'Archivo subido',
        'yes'           => 'Si'
    ],
    'last_login'        => 'Último acceso',
    'none'              => 'Ninguna',
    'ops' => [
        'confirmation'  => '¿Estás seguro?',
        'create'        => 'Crear',
        'delete'        => 'Eliminar',
        'edit'          => 'Editar',
        'modified'      => 'Modificado',
        'name'          => 'Ops',
        'order'         => 'Orden',
        'show'          => 'Mostrar',
    ],
    'root'              => 'Tablero',
    'submit'            => 'Enviar',
    'title'             => 'Panel de control',
    'update' => [
        'fail'          => 'La operación de actualización en el recurso ha fallado.',
        'success'       => "El recurso se ha actualizado correctamente.",
    ],
    'save' => 'Guardar'
];

return createTranslation(require __DIR__ . DIRECTORY_SEPARATOR . 'resources.php', $translation);