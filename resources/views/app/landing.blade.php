@extends('layouts.landing')

@include('partials.app.sections', [
'title' => getTitle($title),
'description' => getDescription($description),
'image' => getImage()
])

@section('content')
    @include('partials.landing.hero')
    @include('partials.landing.about')
    @include('partials.landing.discography')
    {{-- @include('partials.landing.divider') --}}
    {{-- @include('partials.landing.tour') --}}
    @include('partials.landing.gallery')
    @include('partials.landing.news')
    @include('partials.landing.contact')
    <a class="block-top scroll" href="#wrapper">
    <i class="icon-angle-up"></i></a>
@endsection
