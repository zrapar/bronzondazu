<!DOCTYPE html>
<html lang="es" itemscope itemtype="http://schema.org/WebPage">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <meta name="token" content="{{ csrf_token() }}">
    <meta itemprop="name" content="@yield('title')">
    <meta itemprop="description" content="@yield('description')">
    <meta itemprop="image" content="@yield('image')">
    <meta name="rating" content="general">
    <meta name="robots" content="@yield('robots')">
    <meta property="og:locale" content="en_US">
    <meta property="og:url" content="{{ request()->url() }}">
    <meta property="og:title" content="@yield('title')">
    <meta property="og:image" content="@yield('image')">
    <meta property="og:type" content="website">
    <meta name="description" property="og:description" content="@yield('description')">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="@yield('title')">
    <meta name="twitter:description" content="@yield('description')">
    <meta name="twitter:image" content="@yield('image')">
    <meta name="twitter:url" content="{{ request()->url() }}">

    <link rel="apple-touch-icon" sizes="57x57" href="/i/icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/i/icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/i/icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/i/icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/i/icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/i/icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/i/icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/i/icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/i/icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/i/icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/i/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/i/icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/i/icons/favicon-16x16.png">
    <link rel="manifest" href="/i/icons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/i/icons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <meta name="msapplication-config" content="{{ asset('i/icons/browserconfig.xml') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('dist/css/app.css')) }}">
    <link href="https://fonts.googleapis.com/css?family=Dosis:100,300,400,600,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300i,300,400,400i,600,700,800" rel="stylesheet">
    @hasSection('canonical')<link rel="canonical" href="@yield('canonical')">@endif
    <script src="{{ asset(mix('dist/js/app.js')) }}"></script>
    @if (env('APP_ENV') !== 'local' && config('settings.analytics_id') !== null)
        <script async src="https://www.googletagmanager.com/gtag/js?id={{ config('settings.analytics_id') }}"></script>
        <script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', '{{ config('settings.analytics_id') }}', {'anonymize_ip': true});</script>
    @endif
</head>
<body>
@include('partials.landing.preloader')
@yield('content')
@include('partials.app.footer')
<script type="text/javascript" src="{{ asset(mix('dist/js/jquery.flexslider-min.js')) }}"></script>
<script type="text/javascript" src="{{ asset(mix('dist/js/smooth-scroll.js')) }}"></script>
<script type="text/javascript" src="{{ asset(mix('dist/js/jquery.magnific-popup.min.js')) }}"></script>
<script type="text/javascript" src="{{ asset(mix('dist/js/audio.min.js')) }}"></script>
<script type="text/javascript" src="{{ asset(mix('dist/js/twitterFetcher_min.js')) }}"></script>
<script type="text/javascript" src="{{ asset(mix('dist/js/placeholders.min.js')) }}"></script>
<script type="text/javascript" src="{{ asset(mix('dist/js/jquery.countdown.min.js')) }}"></script>
<script type="text/javascript" src="{{ asset(mix('dist/js/script.js')) }}"></script>
@hasSection('scripts')@yield('scripts')@endif
</body>
</html>
