<!--About section-->
<section id="about" class="about overlay main">
    <div class="background-img" >
        <img src="/img/3.jpg" alt="">
    </div>
    <!--Container-->
    <div class="container">
        <!--Row-->
        <div class="row  vertical-align mb-4">
            <div class="col-md-8">
                <div class="front-p">
                    <h1 class="text-center uppercase text-white">Bronzon y Dazu debuta con la canción "Donde Tú Estás"</h1>
                </div>
            </div>
        </div>
        <!--End row-->
        <!--Row-->
        <div class="row  vertical-align">
            <div class="col-md-8">
                <div class="front-p">
                    <p class="text-justify w-93"><span class="font-bold">Bronson Rodríguez y Alex Dávila son primos y por buen tiempo estuvieron preparando su lanzamiento como dúo musical;</span> tras varias reuniones en equipo y de la mano de Poe Polanco (<a href="https://instagram.com/poepolanco" target="_blank" rel="noopener noreferrer">@poepolanco</a>) decidieron debutar en esta época de confinamiento por la pandemia, ello con la intención de llevar alegría al mundo.</p>
                    <p class="text-justify w-93"><span class="font-bold">Como Bronzon y Dazu lanzan su sencillo promocional "Donde tú estás"</span>, un reggaetón el cual fue escrito Eduh “El virus” (<a href="https://instagram.com/Eduh_elvirus" target="_blank" rel="noopener noreferrer">@Eduh_elvirus</a>) mientras que la producción musical estuvo a cargo de Elías Krivoy.</p>
                    <p class="text-justify w-93"><span class="font-bold">Bronzon y Dazu tienen grandes expectativas dentro de la música</span>, entre ellas: “Ser unos grandes exponentes del género urbano y llegar tener la aprobación del público en todos los niveles posibles”, dijeron.</p>
                    <p class="text-justify w-93">Debemos hacer saber que <span class="font-bold">Bronzon y Dazu ya tienen grabados otros temas</span> mismos que irán lanzando según estrategia ya planteada y por supuesto, dependiendo del avance que vaya teniendo la situación de salud a nivel mundial. Adelantaron que los sencillos tienen ritmos variados, entre ellos encontraremos: Reggaetón, R&B, Trap y Dance Hall. </p>
                    <p class="text-justify w-93"><span class="font-bold">Sobre Bronzon y Dazu…</span></p>
                    <p class="text-justify w-93">El nombre del dúo sale de sus nombres y apellidos. <span class="font-bold">Bronzon</span> cambió la S de su nombre de pila por la Z y <span class="font-bold">Dazu</span> es la mezcla de las 2 primeras letras de sus apellidos.</p>
                    <p class="text-justify w-93"><span class="font-bold">Bronzon (<a href="https://www.instagram.com/bronzon_oficial" target="_blank" rel="noopener noreferrer">@Bronzon_oficial</a>)</span> cuenta con 26 años de edad. Estudió actuación en El conservatorio (LAPAC) de Los Ángeles, hace lo propio en la escuela de Raquel Pérez en Madrid. Regresa a Venezuela y durante 2 años tomó clases en la Escuela Luz Columba. También hizo carrera en el modelaje en nuestro país desfilando y siendo imagen de varias marcas; en 2010 ganó el concurso "Joven Teen Venezuela".</p>
                    <p class="text-justify w-93">En cuanto a la música curso estudios en la Academia Jukebox en Madrid y también tomó clases en escuela Musical Blue con la profesora Lagrimita Rodríguez.</p>
                    <p class="text-justify w-93">También se ha destacado en diferentes actividades deportivas como: Karate Do, Lucha y Kata. Actualmente practica boxeo.</p>
                    <p class="text-justify w-93">Desde muy pequeño a <span class="font-bold">Dazu (<a href="https://www.instagram.com/dazu.oficial" target="_blank" rel="noopener noreferrer">@Dazu.oficial</a>)</span> le gustó el canto sin embargo se inició en el medio artístico como modelo. En el año 2008 participó en el concurso de belleza masculino "Míster Young" realizado en República Dominicana y donde clasificó como primer finalista; estuvo en "Joven Venezuela 2010" año en el cual, de manera definitiva, decidió dedicarse a la música grabando covers y subirlo a las redes sociales. Hace un par de años, su vida dio un vuelco total cuando se junta con su primo Bronzon y deciden centrarse en el proyecto del dúo.</p>
                    <p class="text-justify w-93"><span class="font-bold">Para saber los pasos de Bronzon y Dazu síguelos a través de sus redes sociales: En <a href="https://www.instagram.com/bronzonydazu" target="_blank" rel="noopener noreferrer">Instagram</a> y <a href="https://twitter.com/bronzonydazu" target="_blank" rel="noopener noreferrer">Twitter</a> los encuentran como @bronzonydazu. Su canal Youtube: <a href="https://www.youtube.com/channel/UCt1vrVIqoRIDYdcV4RoaKfQ" target="_blank" rel="noopener noreferrer">Bronzon Dazu.</a></span></p>
                    <p class="text-justify w-93"><span class="font-bold">“Donde tú estás” ya se encuentra disponible en todas las plataformas digitales.</span></p>
                    <p class="text-justify w-93"><span class="font-bold">Disfruta de “Sola” aquí: <a href="https://youtu.be/E7-pDo7iLXg" target="_blank" rel="noopener noreferrer">https://youtu.be/E7-pDo7iLXg</a></span></p>
                </div>
            </div>
        </div>
        <!--End row-->
    </div>
    <!--End container-->
</section>
<!--End about section-->
