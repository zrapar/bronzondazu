<section id="tour" class="tour  main bg-secondary">
            <!--Container-->
            <div class="container">
               <!--Row-->
               <div class="row justify-content-center ">
                  <div class="col-12 col-md-10 col-lg-9">
                     <div class="block-content  gap-one-bottom-md text-center">
                        <div class="block-title ">
                           <h1 class="uppercase">Upcoming tours</h1>
                        </div>
                     </div>
                  </div>
               </div>
               <!--End row-->
            </div>
            <!--End container-->
            <!--Container-->
            <div class="container">
               <!--Row-->
               <div class="row justify-content-center ">
                  <div class="col-12 col-lg-8 col-md-10">
                     <ul class="block-tabs list-inline gap-one-bottom-sm text-center">
                        <li class="active list-inline-item">
                           <h5 class="uppercase  mb-0">american tour</h5>
                        </li>
                        <li class="list-inline-item">
                           <h5 class="uppercase  mb-0">europeen tour</h5>
                        </li>
                     </ul>
                     <ul class="block-tab">
                        <!--Tab-->
                        <li class="active ">
                           <div class="block-content text-center">
                              <div class="block-video">
                                 <img src="/img/8.jpg" class="background-img mb-0"  alt="" >
                                 <a class="video-play-but popup-youtube" href="https://www.youtube.com/watch?v=Gc2en3nHxA4"></a>
                                 <div class="embed-responsive embed-responsive-16by9">
                                 </div>
                              </div>
                              <p class=" mt-3"><span class="opc-70">The American Tour 2018 -</span> <a class="link" href="#">Booking Enqueries</a> </p>
                           </div>
                           <div class="block-content gap-one-top-sm text-left">
                              <div class="block-content ">
                                 <div class="row">
                                    <div class="col-lg-3 col-md-3">
                                       <h4 class="switch-fot">14 Mar</h4>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                       <h6 class="mb-0 opc-70 uppercase">Melbourne, AU</h6>
                                       <span>Rod Laver Arena </span>
                                    </div>
                                    <div class="col-12 col-lg-5 col-md-5 text-md-right">
                                       <a class="btn-s uppercase btn btn-primary with-ico border-2" href="#"><i class="icon-ticket"></i>Vip</a>
                                       <a class="btn-s uppercase btn btn-primary with-ico" href="#"><i class="icon-ticket"></i>Buy Ticket</a>
                                    </div>
                                 </div>
                              </div>
                              <hr>
                              <div class="block-content ">
                                 <div class="row">
                                    <div class="col-lg-3 col-md-3">
                                       <h4 class="switch-fot">10 Apr</h4>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                       <h6 class="mb-0 opc-70 uppercase">Washington, DC, USA</h6>
                                       <span>Capital One Arena</span>
                                    </div>
                                    <div class="col-12 col-lg-5 col-md-5 text-md-right">
                                       <a class="btn-s uppercase btn btn-primary with-ico" href="#"><i class="icon-ticket"></i>Buy Ticket</a>
                                    </div>
                                 </div>
                              </div>
                              <hr>
                              <div class="block-content ">
                                 <div class="row">
                                    <div class="col-lg-3 col-md-3">
                                       <h4 class="switch-fot">24 May</h4>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                       <h6 class="mb-0 opc-70 uppercase">Houston, TX, USA</h6>
                                       <span>Arena Theatre</span>
                                    </div>
                                    <div class=" col-12 col-lg-5 col-md-5 text-md-right">
                                       <a class="btn-s uppercase btn btn-primary with-ico border-2" href="#"><i class="icon-ticket"></i>Vip</a>
                                       <a class="btn-s uppercase btn btn-primary with-ico" href="#"><i class="icon-ticket"></i>Buy Ticket</a>
                                    </div>
                                 </div>
                              </div>
                              <hr>
                              <div class="block-content">
                                 <div class="row">
                                    <div class="col-lg-3 col-md-3">
                                       <h4 class="switch-fot">31 Jun</h4>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                       <h6 class="mb-0 opc-70 uppercase">Chicago, IL, USA</h6>
                                       <span>United Center</span>
                                    </div>
                                    <div class="col-12 col-lg-5 col-md-5 text-md-right">
                                       <a class="btn-s uppercase btn btn-primary with-ico" href="#"><i class="icon-ticket"></i>Buy Ticket</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="block-content text-center">
                              <div class="block-video">
                                 <img src="/img/9.jpg" class="background-img mb-0"  alt="" >
                                 <a class="video-play-but popup-youtube" href="https://www.youtube.com/watch?v=Gc2en3nHxA4"></a>
                                 <div class="embed-responsive embed-responsive-16by9">
                                 </div>
                              </div>
                              <p class=" mt-3"><span class="opc-70">The Europeen Tour 2018 -</span> <a class="link" href="#">Booking Enqueries</a> </p>
                           </div>
                           <div class="block-content gap-one-top-sm text-left">
                              <div class="block-content ">
                                 <div class="row ">
                                    <div class="col-lg-3 col-md-3">
                                       <h4 class="switch-fot">24 Jul</h4>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                       <h6 class="mb-0 opc-70 uppercase">Stockholm, Sweden</h6>
                                       <span>Annexet</span>
                                    </div>
                                    <div class="col-12 col-lg-5 col-md-5 text-md-right">
                                       <a class="btn-s uppercase btn btn-primary with-ico" href="#"><i class="icon-ticket"></i>Buy Ticket</a>
                                    </div>
                                 </div>
                              </div>
                              <hr>
                              <div class="block-content ">
                                 <div class="row">
                                    <div class="col-lg-3 col-md-3">
                                       <h4 class="switch-fot">16 Aug</h4>
                                    </div>
                                    <div class=" col-lg-4 col-md-4">
                                       <h6 class="mb-0 opc-70 uppercase">Berlin, Germany</h6>
                                       <span>Mercedes-Benz Arena</span>
                                    </div>
                                    <div class="col-12 col-lg-5 col-md-5 float-left text-md-right">
                                       <a class="btn-s uppercase btn btn-primary with-ico" href="#"><i class="icon-ticket"></i>Buy Ticket</a>
                                    </div>
                                 </div>
                              </div>
                              <hr>
                              <div class="block-content ">
                                 <div class="row">
                                    <div class="col-lg-3 col-md-3">
                                       <h4 class="switch-fot">24 Sep</h4>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                       <h6 class="mb-0 opc-70 uppercase">Monterrey, Mexico</h6>
                                       <span>Machaca </span>
                                    </div>
                                    <div class="col-12 col-lg-5 col-md-5 text-md-right">
                                       <a class="btn-s uppercase btn btn-primary with-ico border-2" href="#"><i class="icon-ticket"></i>Vip</a>
                                       <a class="btn-s uppercase btn btn-primary with-ico" href="#"><i class="icon-ticket"></i>Buy Ticket</a>
                                    </div>
                                 </div>
                              </div>
                              <hr>
                              <div class="block-content">
                                 <div class="row">
                                    <div class="col-lg-3 col-md-3">
                                       <h4 class="switch-fot">31 Oct</h4>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                       <h6 class="mb-0 opc-70 uppercase">Moscow, Russia</h6>
                                       <span>Olimpiyskiy</span>
                                    </div>
                                    <div class="col-12 col-lg-5 col-md-5 text-md-right">
                                       <a class="btn-s uppercase btn btn-primary with-ico border-2" href="#"><i class="icon-ticket"></i>Vip</a>
                                       <a class="btn-s uppercase btn btn-primary with-ico" href="#"><i class="icon-ticket"></i>Buy Ticket</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </li>
                     </ul>
                  </div>
                  <div class="col-12  col-lg-8   col-md-10">
                     <div class="block-content  gap-one-top-sm">
                        <div class="card  bg-red ">
                           <div class="card-body p-5 block-subscribe ">
                              <p class="uppercase text-center mb-4">Subscribe for free downloads and <br>band news updates</p>
                              <form method="get">
                                 <div class="form-row justify-content-center">
                                    <div class="col-12 col-md-9 col-lg-8">
                                       <div class="form-group">
                                          <input class="form-control form-control-lg" name="email" placeholder="Email Address..." type="email">
                                          <span class="text-small mt-1">* We don’t share your information with anyone.</span>
                                       </div>
                                    </div>
                                    <div class="col-auto">
                                       <button type="submit" class="btn  btn-primary uppercase border-3">
                                       Subscribe now</button>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!--End row-->
            </div>
            <!--End container-->
         </section>
