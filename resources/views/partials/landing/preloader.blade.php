<div class="loader">
         <!-- Preloader inner -->
         <div class="loader-inner">
            <svg width="120" height="220" viewbox="0 0 100 100" class="loading-spinner" version="1.1" xmlns="http://www.w3.org/2000/svg">
               <circle class="spinner" cx="50" cy="50" r="21" fill="#13181d" stroke-width="2"/>
               {{-- <circle class="spinner" cx="50" cy="50" r="40" stroke="black" stroke-width="3" fill="red" /> --}}
            </svg>
         </div>
         <!-- End preloader inner -->
      </div>
