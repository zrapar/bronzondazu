<section  class="divider overlay">
    <div class="background-img" >
        <img  src="/img/4.jpg" alt="">
    </div>
    <!--Container-->
    <div class="container">
        <!--Row-->
        <div class="row justify-content-center">
            <div class="col-12 col-lg-10 ">
                <div class="block-content text-center front-p">
                    <h1 class="uppercase">Time left until the upcoming tour </h1>
                    <p class="lead">27 to 31 July 2018 with over 10 show - Cincinnati, Ohio  </p>
                    <span id="countdown" class="countdown uppercase  mb-0 opc-70"></span>
                </div>
            </div>
        </div>
        <!--End row-->
    </div>
    <!--End container-->
</section>
