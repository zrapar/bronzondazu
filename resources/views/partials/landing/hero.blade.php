@include('partials.app.errors')
<section class="hero">
            <!--Main slider-->
    <div class="main-slider slider flexslider">
        <ul class="slides">
            <li>
                <div class="background-img overlay zoom">
                <img src="/img/main.jpg" alt="">
                </div>
                <!--Container-->
                <div class="container hero-content">
                <!--Row-->
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <!--Inner hero-->
                        <div class="inner-hero">
                            <img class="center-logo" src="/img/logo-blanco.png" alt="Logo" />
                            {{-- <div class="back-rect"></div> --}}
                            {{-- <h1 class="large text-white uppercase mb-0">BRONZON & DAZU</h1> --}}
                            {{-- <h5 class="mb-0 text-white uppercase">Music Band and Musician Bootstrap Template</h5> --}}
                            {{-- <div class="front-rect"></div> --}}
                        </div>
                    </div>
                    <!--End row-->
                </div>
                <!--End container-->
                </div>
                <!--End inner hero-->
            </li>
            <li>
                <div class="background-img overlay zoom">
                <img src="/img/sola.jpg" alt="">
                </div>
                <!--Container-->
                <div class="container hero-content">
                <!--Row-->
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <!--Inner hero-->
                        <div class="inner-hero">
                            <h1 class="large text-white uppercase mb-0">Sola</h1>
                            <h5 class="mb-0 text-white uppercase">Vídeo Oficial</h5>
                            <a class="video-play-but popup-youtube" href="https://www.youtube.com/watch?v=E7-pDo7iLXg"></a>
                        </div>
                    </div>
                    <!--End row-->
                </div>
                <!--End container-->
                </div>
                <!--End inner hero-->
            </li>
        </ul>
    </div>
    <!--End main slider-->
    <!--Header-->
    <header class="header default">
        <div class=" left-part">
            <a class="logo scroll" href="#wrapper">
                <h2 class="mb-0 uppercase">BRONZON & DAZU</h2>
            </a>
        </div>
        <div class="right-part">
            <nav class="main-nav">
                <div class="toggle-mobile-but">
                <a href="#" class="mobile-but" >
                    <div class="lines"></div>
                </a>
                </div>
                <ul class="main-menu list-inline">
                <li><a class="scroll list-inline-item" href="#wrapper">Inicio</a></li>
                <li><a class="scroll list-inline-item" href="#about">Biografía</a></li>
                <li><a class="scroll list-inline-item" href="#discography">Música</a></li>
                <li><a class="scroll list-inline-item" href="#tour">Shows</a></li>
                <li><a class="scroll list-inline-item" href="#gallery">Galería</a></li>
                <li><a class="scroll list-inline-item" href="#news">Noticias</a></li>
                <li><a class="scroll list-inline-item" href="#contact">Contacto</a></li>
                </ul>
            </nav>
        </div>
    </header>
    <!--End header-->
</section>
