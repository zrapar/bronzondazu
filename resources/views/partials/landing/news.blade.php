@if($articles->isEmpty())
<section id="news" class="news main bg-secondary">
    <!--Container-->
    <div class="container">
        <!--Row-->
        <div class="row justify-content-center ">
            <div class="col-12 col-md-10 col-lg-9">
                <div class="block-content  gap-one-bottom-md text-center">
                    <div class="block-title ">
                        <h1 class="uppercase">Noticias</h1>
                    </div>
                </div>
            </div>
        </div>
        <!--End row-->
    </div>
    <!--End container-->
    <!--Container-->
    <div class="container">
        <ul class="news-list">
            <li class="row align-items-center justify-content-around">

                <div class="col-12   order-md-1 ">
                    <div class="block-content text-left">

                        <h2 class="text-center" >No hay articulos publicados aun</h2>


                    </div>
                </div>
            </li>

        </ul>

    </div>
    <!--End container-->
</section>
@else
<section id="news" class="news main bg-secondary">
    <!--Container-->
    <div class="container">
        <!--Row-->
        <div class="row justify-content-center ">
            <div class="col-12 col-md-10 col-lg-9">
                <div class="block-content  gap-one-bottom-md text-center">
                    <div class="block-title ">
                        <h1 class="uppercase">Noticias</h1>
                    </div>
                </div>
            </div>
        </div>
        <!--End row-->
    </div>
    <!--End container-->
    <!--Container-->
    <div class="container">
        <ul class="news-list">
            @foreach ($articles as $key => $article)
                <li class="row align-items-center justify-content-around">
                    <div class="col-12 col-md-6 order-md-2">
                        <div class="block-content">
                            <a href="{{route('article', ['articleSlug' => $article['slug']])}}"><img alt="" class="img-fluid animated" src="{{$article['image']}}"></a>
                        </div>
                    </div>
                    @if($key == '0')
                        <div class="col-12 col-md-6 col-lg-5 order-md-1 ">
                    @else
                        <div class="col-12 col-md-6 col-lg-5  order-2 text-left">
                    @endif
                        <div class="block-content text-left">
                            <span class="mb-2 opc-70">{{\Carbon\Carbon::parse($article['published_at'])->format('Y-m-d')}}</span>
                            <h2 >{{$article['title']}}</h2>
                            <p class="lead">
                                {{$article['description']}}
                            </p>
                            <a href="{{route('article', ['articleSlug' => $article['slug']])}}">Lee más ›</a>
                        </div>
                    </div>
                </li>

            @endforeach
        </ul>
        <div class="block-content text-center">
            <a class="btn btn-primary with-ico uppercase mt-5 " href="{{ route('news')}}">Ver todas las noticias</a>
        </div>
    </div>
    <!--End container-->
</section>
@endif

