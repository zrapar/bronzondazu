<!--Discography section-->
<section id="discography" class="discography main">
    <!--Container-->
    <div class="container">
        <!--Row-->
        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-lg-9 ">
                <div class="block-content text-center gap-one-bottom-md">
                    <div class="block-title ">
                        <h1 class="uppercase">Música</h1>
                    </div>
                </div>
            </div>
        </div>
        <!--End row-->
    </div>
    <!--End container-->
    <!--Container-->
    <div class="container">
        <!--Row-->
        <div class="row gap-one-bottom-sm">
            <div class="col-12 col-md-6 col-lg-4">
                <div class="block-album block-content">
                    <h5 class="mb-0 opc-70 uppercase mb-2">Sola</h5>
                    <img class="animated" src="/img/sola-flyer.jpg" alt="Sola">

                    <ul class="block-social list-inline mb-md-3">
                        <li class="list-inline-item mr-0"><a href="#"><i class="socicon-apple"></i></a></li>
                        <li class="list-inline-item mr-0"><a href="#"><i class="socicon-play"></i></a></li>
                        <li class="list-inline-item mr-0"><a href="#"><i class="socicon-amazon"></i></a></li>
                        <li class="list-inline-item mr-0"><a href="#"><i class="socicon-soundcloud"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4">
                <div class="block-album block-content">
                    <h5 class="mb-0 opc-70 uppercase mb-2">Donde tu estas</h5>
                    <img class="animated" src="/img/donde-tu-estas-flyer.jpeg" alt="Donde tu estas">

                    <ul class="block-social list-inline mb-md-3">
                        <li class="list-inline-item mr-0"><a href="#"><i class="socicon-apple"></i></a></li>
                        <li class="list-inline-item mr-0"><a href="#"><i class="socicon-play"></i></a></li>
                        <li class="list-inline-item mr-0"><a href="#"><i class="socicon-amazon"></i></a></li>
                        <li class="list-inline-item mr-0"><a href="#"><i class="socicon-soundcloud"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4">
                <div class="block-album block-content">
                    <h5 class="mb-0 opc-70 uppercase mb-2">No lo pienses</h5>
                    <img  class="animated" src="/img/no-lo-pienses-flyer.jpg" alt="No lo pienses">

                    <ul class="block-social list-inline mb-md-3">
                        <li class="list-inline-item mr-0"><a href="#"><i class="socicon-apple"></i></a></li>
                        <li class="list-inline-item mr-0"><a href="#"><i class="socicon-play"></i></a></li>
                        <li class="list-inline-item mr-0"><a href="#"><i class="socicon-amazon"></i></a></li>
                        <li class="list-inline-item mr-0"><a href="#"><i class="socicon-soundcloud"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!--End row-->
    </div>
    <!--End container-->
    <!--Container-->
    <div class="container">
        <!--Row-->
        <div class="row justify-content-center">
            <div class="col-12 col-lg-8 col-md-10">
                <div class="block-tracklist">
                    <audio preload class="album"></audio>
                    <ol class="playlist">
                        <li >
                            <div class="as-link" data-src="/i/uploads/mp3/sola.mp3">
                                <!--Row-->
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="block-track">
                                            <h6 class="mb-0 opc-70 uppercase ">Sola</h6>
                                            <span>Bronzon & Dazu </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 text-md-right">
                                        <a href="#" class="btn-s uppercase btn btn-primary with-ico border-2 toggle-lyrics"><i class="icon-note"></i>Letra</a>
                                    </div>
                                    <div class="col-12 ">
                                        <div class="block-lyrics w-75 text-center mt-3">
                                            <h5 class="mb-4 opc-70 uppercase ">Sola</h5>
                                            <p>Siempre llamando después de las 3
                                                Pa que te busque después donde la otra vez
                                                Yo soy el cómplice
                                                Conmigo no tienes límites
                                            </p>
                                            <p>Yo no soy tuyo tu tampoco mia
                                                Tu tienes novio pero el te descuida
                                                Yo si te cumplo lo que el prometía
                                                Avisame y te busco como el otro día
                                            </p>
                                            <p>Para que no te sientas sola
                                                Lo nuestro no pasa de moda
                                                Si nos buscamos, nos encontramos
                                                Bebe tu solo indicame la hora
                                                Bis
                                            </p>
                                            <p>Si no te sigo no me sigues
                                                No nos damos ni like
                                                Siempre la dejo por la morning
                                                Aunque la busque en la night
                                                Yeah, al novio le dijo Good bye
                                                A mi me dice baby ya tu sabe la que hay
                                            </p>
                                            <p>El bobo ese la solo y ella me dijo hola
                                                Que Eran más de las 2 y que estaba sola
                                                Que le tenia temor a la oscuridad
                                                Que si le podía llegar
                                            </p>
                                            <p>Para que no te sientas sola
                                                Lo nuestro no pasa de moda
                                                Si nos buscamos, nos encontramos
                                                Bebe tu solo indicame la hora
                                                Bis
                                            </p>
                                            <p>Lo de nosotros es inusual
                                                Tamos para pecar
                                                Yummy yummy
                                                Te quiero probar
                                                Dejate llevar q el no va a llegarte
                                                Desnudate y empieza a provocarme
                                                Tu tiene el don para descontrolarme
                                            </p>
                                            <p>El bobo ese la solo y ella me dijo hola
                                                Que Eran más de las 2 y que estaba sola
                                                Que le tenia temor a la oscuridad
                                                Que si le podía llegar
                                            </p>
                                            <p>Siempre llamando después de las 3
                                                Pa que te busque después donde la otra vez
                                                Yo soy el cómplice
                                                Conmigo no tienes límites
                                            </p>
                                            <p>Yo no soy tuyo tu tampoco mia
                                                Tu tienes novio pero el te descuida
                                                Yo si te cumplo lo que el prometía
                                                Avisame y te busco como el otro día
                                            </p>
                                            <p>Para que no te sientas sola
                                                Lo nuestro no pasa de moda
                                                Si nos buscamos, nos encontramos
                                                Bebe tu solo indicame la hora
                                                Bis
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li >
                            <div class="as-link" data-src="/i/uploads/mp3/donde-tu-estas.mp3">
                                <!--Row-->
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="block-track">
                                            <h6 class="mb-0 opc-70 uppercase ">Donde tu estas</h6>
                                            <span> Bronzon & Dazu</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 text-md-right">
                                        <a href="#" class="btn-s uppercase btn btn-primary with-ico border-2 toggle-lyrics"><i class="icon-note"></i>Letra</a>
                                    </div>
                                    <div class="col-12 ">
                                        <div class="block-lyrics w-75 text-center mt-3">
                                            <h5 class="mb-4 opc-70 uppercase ">Donde tu estas</h5>
                                            <p>Creo que ya te perdí
                                                Que fui un idiota y mucho más
                                                Y ahora que no estás aquí
                                                Siento que eras mi otra mitad
                                            </p>
                                            <p>Si tan solo pudiera
                                                Echar el tiempo pa atrás
                                            </p>
                                            <p>Nunca te haría llorar</p>
                                            <p>Dime dónde tu estás
                                                Que yo necesito de tu piel
                                                La verdad sin ti es diferente
                                            </p>
                                            <p>Juro que no sales de mi mente
                                                Y solo pido verte una vez más
                                            </p>
                                            <p>Que yo necesito de tu piel
                                                La verdad sin ti to es diferente
                                            </p>
                                            <p>Juro que no sales de mi mente
                                                Y solo pido verte una vez más
                                                Bis
                                            </p>
                                            <p>Ahora que no estamos juntos
                                                Se me apagó la luz y ese es el punto
                                                Que de mi vida tu eras el mundo
                                                Y siento que me hundo
                                                Cuando a la luna yo por ti pregunto
                                                Sabiendo que ahora soy un moribundo
                                            </p>
                                            <p>Y ahora que te marchaste
                                                Lo poco que quedaba te llevaste
                                                Pregunto si es que acaso me olvidaste
                                                O si piensas en mi
                                                Y si te pasa igual q a mi
                                                Entonces déjame saber y
                                            </p>
                                            <p>Dime dónde tu estás
                                                Que yo necesito de tu piel
                                                La verdad sin ti to es diferente
                                            </p>
                                            <p>Juro que no sales de mi mente
                                                Y solo pido verte una vez más
                                            </p>
                                            <p>Que yo necesito de tu piel
                                                La verdad sin ti to es diferente
                                            </p>
                                            <p>Juro que no sales de mi mente
                                                Y solo pido verte
                                            </p>
                                            <p>Me la pasó toda las noches
                                                Pensando ti
                                                Buscando la manera de llegar a ti
                                            </p>
                                            <p>Te fallé, si.
                                                Yo lo entendí, pero
                                                Te hablo claro me arrepentí
                                            </p>
                                            <p>Ya nada me sale bien
                                                Porque no estás aquí
                                                Dicen que me superaste
                                                Y yo sigo aquí
                                                Llorando por ti
                                                Y tu feliz
                                                No supe lo que tenía
                                                hasta que te perdí
                                            </p>
                                            <p>Creo que ya te perdí
                                                Que fui un idiota y mucho más
                                                Y ahora que no estás aquí
                                                Siento que eras mi otra mitad
                                            </p>
                                            <p>Si tan solo pudiera
                                                Echar el tiempo pa atrás
                                            </p>
                                            <p>Nunca te haría llorar</p>
                                            <p>Dime dónde tu estás
                                                Que yo necesito de tu piel
                                                La verdad sin ti to es diferente
                                            </p>
                                            <p>Juro que no sales de mi mente
                                                Y solo pido verte una vez más
                                            </p>
                                            <p>Que yo necesito de tu piel
                                                La verdad sin ti to es diferente
                                            </p>
                                            <p>Juro que no sales de mi mente
                                                Y solo pido verte una vez más
                                                Bis
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li >
                            <div class="as-link" data-src="/i/uploads/mp3/no-lo-pienses.mp3">
                                <!--Row-->
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="block-track">
                                            <h6 class="mb-0 opc-70 uppercase ">No lo pienses</h6>
                                            <span>Bronzon Ft. Eduh "El Virus"</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 text-md-right">
                                        <a href="#" class="btn-s uppercase btn btn-primary with-ico border-2 toggle-lyrics"><i class="icon-note"></i>Letra</a>
                                    </div>
                                    <div class="col-12 ">
                                        <div class="block-lyrics w-75 text-center mt-3">
                                            <h5 class="mb-4 opc-70 uppercase ">No lo pienses</h5>
                                            <p>Creo que ya te perdí
                                                Que fui un idiota y mucho más
                                                Y ahora que no estás aquí
                                                Siento que eras mi otra mitad
                                            </p>
                                            <p>Si tan solo pudiera
                                                Echar el tiempo pa atrás
                                            </p>
                                            <p>Nunca te haría llorar</p>
                                            <p>Dime dónde tu estás
                                                Que yo necesito de tu piel
                                                La verdad sin ti es diferente
                                            </p>
                                            <p>Juro que no sales de mi mente
                                                Y solo pido verte una vez más
                                            </p>
                                            <p>Que yo necesito de tu piel
                                                La verdad sin ti to es diferente
                                            </p>
                                            <p>Juro que no sales de mi mente
                                                Y solo pido verte una vez más
                                                Bis
                                            </p>
                                            <p>Ahora que no estamos juntos
                                                Se me apagó la luz y ese es el punto
                                                Que de mi vida tu eras el mundo
                                                Y siento que me hundo
                                                Cuando a la luna yo por ti pregunto
                                                Sabiendo que ahora soy un moribundo
                                            </p>
                                            <p>Y ahora que te marchaste
                                                Lo poco que quedaba te llevaste
                                                Pregunto si es que acaso me olvidaste
                                                O si piensas en mi
                                                Y si te pasa igual q a mi
                                                Entonces déjame saber y
                                            </p>
                                            <p>Dime dónde tu estás
                                                Que yo necesito de tu piel
                                                La verdad sin ti to es diferente
                                            </p>
                                            <p>Juro que no sales de mi mente
                                                Y solo pido verte una vez más
                                            </p>
                                            <p>Que yo necesito de tu piel
                                                La verdad sin ti to es diferente
                                            </p>
                                            <p>Juro que no sales de mi mente
                                                Y solo pido verte
                                            </p>
                                            <p>Me la pasó toda las noches
                                                Pensando ti
                                                Buscando la manera de llegar a ti
                                            </p>
                                            <p>Te fallé, si.
                                                Yo lo entendí, pero
                                                Te hablo claro me arrepentí
                                            </p>
                                            <p>Ya nada me sale bien
                                                Porque no estás aquí
                                                Dicen que me superaste
                                                Y yo sigo aquí
                                                Llorando por ti
                                                Y tu feliz
                                                No supe lo que tenía
                                                hasta que te perdí
                                            </p>
                                            <p>Creo que ya te perdí
                                                Que fui un idiota y mucho más
                                                Y ahora que no estás aquí
                                                Siento que eras mi otra mitad
                                            </p>
                                            <p>Si tan solo pudiera
                                                Echar el tiempo pa atrás
                                            </p>
                                            <p>Nunca te haría llorar</p>
                                            <p>Dime dónde tu estás
                                                Que yo necesito de tu piel
                                                La verdad sin ti to es diferente
                                            </p>
                                            <p>Juro que no sales de mi mente
                                                Y solo pido verte una vez más
                                            </p>
                                            <p>Que yo necesito de tu piel
                                                La verdad sin ti to es diferente
                                            </p>
                                            <p>Juro que no sales de mi mente
                                                Y solo pido verte una vez más
                                                Bis
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <!--End row-->
    </div>
    <!--End container-->
</section>
<!--End discography section-->
