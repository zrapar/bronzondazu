<section id="gallery" class="gallery main bg-secondary">
    <!--Container-->
    <div class="container">
        <!--Row-->
        <div class="row justify-content-center ">
            <div class="col-12 col-md-10 col-lg-9">
                <div class="block-content  gap-one-bottom-md text-center">
                    <!-- As a general rule, include a heading (h1-h6) as a child of each section and article element for screen readers purposes-->
                    <h1 class="uppercase indent">Upcoming tours</h1>
                    <i class="icon-camera-7 big-icon adjust-space "></i>
                </div>
            </div>
        </div>
        <!--End row-->
    </div>
    <!--End container-->
    <!--Container-->
    <div class="container">
        <!--Row-->
        <div class="row justify-content-center text-center">
            <div class="col-12 ">
                <div class="card-gallery image-gallery">
                    {{-- @foreach ($gallery as $img)
                        <a href="{{ "$url_gallery$img" }}" class="popup-image mb-0"><img class="animated" alt="" src="{{ "$url_gallery$img" }}" ></a>
                    @endforeach --}}
                    <a href="/img/gallery/foto-1.jpg" class="popup-image mb-0"><img class="animated" alt="" src="/img/gallery/foto-1.jpg" ></a>
                    <a href="/img/gallery/foto-2.jpg" class="popup-image mb-0"><img class="animated" alt="" src="/img/gallery/foto-2.jpg" ></a>
                    <a href="/img/gallery/foto-3.jpg" class="popup-image mb-0"><img class="animated" alt="" src="/img/gallery/foto-3.jpg" ></a>
                    <a href="/img/gallery/foto-4.jpg" class="popup-image mb-0"><img class="animated" alt="" src="/img/gallery/foto-4.jpg" ></a>
                    <a href="/img/gallery/foto-5.jpg" class="popup-image mb-0"><img class="animated" alt="" src="/img/gallery/foto-5.jpg" ></a>
                    <a href="/img/gallery/foto-6.jpg" class="popup-image mb-0"><img class="animated" alt="" src="/img/gallery/foto-6.jpg" ></a>
                    <a href="/img/gallery/foto-7.jpg" class="popup-image mb-0"><img class="animated" alt="" src="/img/gallery/foto-7.jpg" ></a>
                    <a href="/img/gallery/foto-8.jpg" class="popup-image mb-0"><img class="animated" alt="" src="/img/gallery/foto-8.jpg" ></a>
                    <a href="/img/gallery/foto-9.jpg" class="popup-image mb-0"><img class="animated" alt="" src="/img/gallery/foto-9.jpg" ></a>
                    <a href="/img/gallery/foto-10.jpg" class="popup-image mb-0"><img class="animated" alt="" src="/img/gallery/foto-10.jpg" ></a>
                    <a href="/img/gallery/foto-11.jpg" class="popup-image mb-0"><img class="animated" alt="" src="/img/gallery/foto-11.jpg" ></a>
                    <a href="/img/gallery/foto-12.jpg" class="popup-image mb-0"><img class="animated" alt="" src="/img/gallery/foto-12.jpg" ></a>
                    <a href="/img/gallery/foto-13.jpg" class="popup-image mb-0"><img class="animated" alt="" src="/img/gallery/foto-13.jpg" ></a>
                    <a href="/img/gallery/foto-14.jpg" class="popup-image mb-0"><img class="animated" alt="" src="/img/gallery/foto-14.jpg" ></a>
                    <a href="/img/gallery/foto-15.jpg" class="popup-image mb-0"><img class="animated" alt="" src="/img/gallery/foto-15.jpg" ></a>
                    <a href="/img/gallery/foto-16.jpg" class="popup-image mb-0"><img class="animated" alt="" src="/img/gallery/foto-16.jpg" ></a>
                    <a href="/img/gallery/foto-17.jpg" class="popup-image mb-0"><img class="animated" alt="" src="/img/gallery/foto-17.jpg" ></a>
                </div>
                <a class="btn btn-primary uppercase with-ico mt-5" href="https://instagram.com/Bronzon_oficial" target="_blank"><i class="icon-instagram"></i>Sigue a @Bronzon_oficial</a>
                <a class="btn btn-primary uppercase with-ico mt-5" href="https://instagram.com/Dazu.oficial" target="_blank"><i class="icon-instagram"></i>Sigue a @Dazu.oficial</a>
                <a class="btn btn-primary uppercase with-ico mt-5" href="https://instagram.com/bronzonydazu" target="_blank"><i class="icon-instagram"></i>Sigue a @bronzonydazu</a>
                <a class="btn btn-primary uppercase with-ico mt-5" href="https://facebook.com/BronzonDazu-110772200685726" target="_blank"><i class="icon-facebook"></i>Dale me gusta en Facebook</a>
            </div>
        </div>
        <!--End row-->
    </div>
    <!--End container-->
</section>
