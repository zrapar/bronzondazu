<section id="band" class="band main ">
            <!--Container-->
            <div class="container">
               <!--Row-->
               <div class="row justify-content-center">
                  <div class="col-12 col-md-10 col-lg-9">
                     <div class="block-content text-center gap-one-bottom-md">
                        <div class="block-title ">
                           <h1 class="uppercase">Band members</h1>
                        </div>
                     </div>
                  </div>
               </div>
               <!--End row-->
            </div>
            <!--End container-->
            <!--Container-->
            <div class="container">
               <!--Row-->
               <div class="row">
                  <div class="col-md-4 col-lg-4">
                     <div class="block-member">
                        <img src="img/5.jpg" alt="">
                        <div class="member-info">
                           <h6 class="uppercase mb-0 ">Joe Walker</h6>
                           <span class=" mt-0"> Lead vocals,guitars</span>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4 col-lg-4">
                     <div class="block-member">
                        <img src="img/6.jpg" alt="">
                        <div class="member-info">
                           <h6 class="uppercase mb-0 ">Andrew Smith</h6>
                           <span class=" mt-0"> Lead guitar, bass guitar</span>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4 col-lg-4">
                     <div class="block-member">
                        <img src="img/7.jpg" alt="">
                        <div class="member-info">
                           <h6 class="uppercase mb-0 ">Jeremy Anderton</h6>
                           <span class=" mt-0">Drums, keyboards</span>
                        </div>
                     </div>
                  </div>
               </div>
               <!--End row-->
            </div>
            <!--End container-->
         </section>
