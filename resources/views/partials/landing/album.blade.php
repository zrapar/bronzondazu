<!--Latest album section-->
<section id="album" class="latest main">
    <!--Container-->
    <div class="container">
    <!--Row-->
    <div class="row justify-content-center">
        <div class="col-12 col-md-10 col-lg-9 ">
                <div class="block-content text-center">
                    <div class="block-title ">
                        <h1 class="uppercase">Música</h1>
                    </div>
                </div>
            </div>
        </div>
        <!--End row-->
    </div>
    <!--End container-->
    <!--Container-->
    <div class="container">
        <!--Row-->
        <div class="row justify-content-center">
            <div class="col-12  col-lg-4  ">
                <div class="block-content text-center gap-one-bottom-sm">
                    <ul class="block-social list-inline mt-4">
                        <li class="list-inline-item mr-0"><a href="#"><i class="socicon-apple"></i></a></li>
                        <li class="list-inline-item mr-0"><a href="#"><i class="socicon-play"></i></a></li>
                        <li class="list-inline-item mr-0"><a href="#"><i class="socicon-amazon"></i></a></li>
                        <li class="list-inline-item mr-0"><a href="#"><i class="socicon-soundcloud"></i></a></li>
                        <li class="list-inline-item mr-0"><a href="#"><i class="socicon-spotify"></i></a></li>
                        <li class="list-inline-item mr-0"><a href="https://www.youtube.com/channel/UCt1vrVIqoRIDYdcV4RoaKfQ" target="_blank"><i class="socicon-youtube"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!--End row-->
    </div>
    <!--End container-->

</section>
<!--End latest album section-->
