<section id="contact" class="contact main bg-secondary">
            <!--Container-->
            <div class="container">
               <!--Row-->
               <div class="row justify-content-center ">
                  <div class="col-12 col-md-10 col-lg-9">
                     <div class="block-content  gap-one-bottom-md text-center">
                        <div class="block-title ">
                           <h1 class="uppercase">Contacto</h1>
                        </div>
                     </div>
                  </div>
               </div>
               <!--End row-->
            </div>
            <!--End container-->
            <!--Container-->
            <div class="container">
               <div class="row justify-content-center">
                  <div class="col-12 col-lg-10">
                     <ul class="feature-list feature-list-sm text-center row">
                        <li class="col-md-6  col-lg-6">
                           <div class="card text-center" >
                              <div class="card-body">
                                 <h2 class="uppercase ">Oficial</h2>
                                 <p class="mb-0">
                                   <h5 class="uppercase opc-70">Bronzon & Dazu</h5>

                                    Bronzonydazu@gmail.com
                                 </p>
                              </div>
                           </div>
                        </li>
                        <li class="col-md-6  col-lg-6">
                           <div class="card text-center">
                              <div class="card-body">
                                 <h2 class="uppercase">Prensa</h2>
                                 <p class="mb-0"><h5 class="uppercase opc-70">Jennifer Suarez</h5>

                                    jennifersuarezg@gmail.com
                                 </p>
                              </div>
                           </div>
                        </li>

                     </ul>
                  </div>
               </div>
               <div class="row justify-content-center">
                  <div class="col-12">
                     <ul class="block-social list-inline text-center">
                        <li class="list-inline-item">
                           <a href="www.facebook.com/BronzonDazu-110772200685726" target="_blank" rel="noopener noreferrer"> <i class="socicon-facebook"></i> </a>
                        </li>
                        <li class="list-inline-item">
                           <a href="https://www.instagram.com/bronzonydazu" target="_blank" rel="noopener noreferrer"><i class="socicon-instagram"></i> </a>
                        </li>
                        <li class="list-inline-item">
                           <a href="https://twitter.com/bronzonydazu" target="_blank" rel="noopener noreferrer"><i class="socicon-twitter"></i> </a>
                        </li>
                        <li class="list-inline-item">
                           <a href="https://www.youtube.com/channel/UCt1vrVIqoRIDYdcV4RoaKfQ" target="_blank" rel="noopener noreferrer"><i class="socicon-youtube"></i> </a>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
            <!--End container-->
         </section>
