<section class="shop main bg-secondary">
            <!--Container-->
            <div class="container">
               <!--Row-->
               <div class="row justify-content-center ">
                  <div class="col-12 col-md-10 col-lg-9">
                     <div class="block-content  gap-one-bottom-md text-center">
                        <div class="block-title ">
                           <h1 class="uppercase">merchandise store</h1>
                        </div>
                     </div>
                  </div>
               </div>
               <!--End row-->
            </div>
            <!--End container-->
            <!--Container-->
            <div class="container">
               <!--Row-->
               <div class="row justify-content-center">
                  <div class="col-12 col-md-6 col-lg-5">
                     <div class="block-content">
                        <a href="#"><img alt="" class="img-fluid animated" src="img/shop.jpg"></a>
                        <a class="btn btn-primary with-ico uppercase w-100 text-center" href="#">
                        shop merchandise</a>
                     </div>
                  </div>
               </div>
               <!--End row-->
            </div>
            <!--End container-->
         </section>
