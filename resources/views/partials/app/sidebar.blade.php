<div class="col-lg-4">
	<div class="sidebar">
		<h3 class="sidebar-title">Categorias</h3>
		<div class="sidebar-item categories">
			<ul>
                @forelse ($categories as $category)
                    <li><a href="{{$category->link}}">{{$category->title}} <span>({!! $category->articles_count() !!})</span></a></li>
                @empty
                    <li>No hay categorias publicadas</li>
                @endforelse

			</ul>
        </div>

		<!-- End sidebar categories-->
		<h3 class="sidebar-title">Noticias Recientes</h3>
		<div class="sidebar-item recent-posts">
            @forelse ($recent_posts as $post)
                <div class="post-item clearfix">
                    <img src="{{$post->thumbnail}}" alt="{{$post->title}}">
                    <h4><a href={{$post->link}}>{{$post->title}}</a></h4>
                    <time datetime="2020-01-01">{{\Carbon\Carbon::parse($post->published_at)->format('Y-m-d')}}</time>
                </div>
            @empty
                <h4>No hay publicaciones Recientes</h4>
            @endforelse

		</div>
		<!-- End sidebar recent posts-->
	</div>
	<!-- End sidebar -->
</div>
