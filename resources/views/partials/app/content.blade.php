<div id="blog-content" class="container mb-2">
   <div class="row">
      <div class="d-none d-lg-block d-xl-block col-lg-12 center-flex">
         <div class="jumbotron jumbotron-fluid hero-image" data-img="{{ $image }}">
            <div class="container">
               <h1 class="text-center display-4">{{$title}}</h1>
               @include('partials.app.breadcrumbs')
            </div>
         </div>
      </div>
      <section class="blog" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
         <div class="container">
            <div class="row">
               <div class="col-lg-8 entries">
                  <article class="entry entry-single">
                     <div class="d-lg-none d-xl-none entry-img">
                        <img src="{{ $image }}" alt="" class="img-fluid">
                     </div>
                     <h2 class="d-lg-none d-xl-none entry-title">
                        <a href="blog-single.html">{{$title}}</a>
                     </h2>
                     <div class="d-lg-none d-xl-none entry-meta">
                        @include('partials.app.breadcrumbs')
                     </div>
                     <div class="entry-content">{!! $content !!}</div>
                  </article>
               </div>
               @include('partials.app.sidebar')
               <!-- End blog sidebar -->
            </div>
         </div>
      </section>
   </div>
</div>
