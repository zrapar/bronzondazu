@include('partials.app.errors')
    <!--Header-->
    <header class="header default">
        <div class=" left-part">
            <a class="logo scroll" href="{{ route('root') }}">
                <h2 class="mb-0 uppercase">BRONZON & DAZU</h2>
            </a>
        </div>
        <div class="right-part">
            <nav class="main-nav">
                <div class="toggle-mobile-but">
                <a href="#" class="mobile-but" >
                    <div class="lines"></div>
                </a>
                </div>
                <ul class="main-menu list-inline">
                <li><a class="scroll list-inline-item" href="{{ route('root') }}">Inicio</a></li>
                <li><a class="scroll list-inline-item" href="{{ route('root') }}#about">Biografía</a></li>
                <li><a class="scroll list-inline-item" href="{{ route('root') }}#discography">Música</a></li>
                <li><a class="scroll list-inline-item" href="{{ route('root') }}#tour">Shows</a></li>
                <li><a class="scroll list-inline-item" href="{{ route('root') }}#gallery">Galería</a></li>
                <li><a class="scroll list-inline-item" href="{{ route('root') }}#news">Noticias</a></li>
                <li><a class="scroll list-inline-item" href="{{ route('root') }}#contact">Contacto</a></li>
                </ul>
            </nav>
        </div>
    </header>
    <!--End header-->
</section>
