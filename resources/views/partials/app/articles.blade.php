<section id="articles" class="blog" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 entries">
                @forelse ($articles as $article)
                    <article class="entry">
                        <div class="entry-img">
                            <img src="{{ $article->image }}" alt="" class="img-fluid">
                        </div>
                        <h2 class="entry-title">
                            <a href="{{ $article->link }}">{{ $article->title }}</a>
                        </h2>
                        <div class="entry-meta">
                            <ul>
                                <li class="d-flex align-items-center">
                                    <i class="icofont-wall-clock"></i>
                                    <a href="{{ $article->link }}">
                                        <time datetime="2020-01-01">{{ $article->localized_published_at }}</time>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="entry-content">
                            <p> {{ getNWords($article->content, 50) }} </p>
                            <div class="read-more">
                                <a href="{{ $article->link }}">Leer más</a>
                            </div>
                        </div>
                    </article>
                @empty
                    <article class="entry">
                        <h2 class="entry-title">
                            No hay noticias publicadas
                        </h2>
                    </article>
                @endforelse

                @if ($articles->total() > $articles->count())
                    <div class="col-md-12">
                        {!! $articles->appends(request()->except('page'))->links() !!}
                    </div>
                @endif
            </div>
            @include('partials.app.sidebar')
        </div>
    </div>
</section>
