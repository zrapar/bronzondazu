<nav aria-label="breadcrumb">
  <ol class="breadcrumb transparent-bg center-flex">
    <li class="breadcrumb-item"><a href="{{ route('root') }}">{{ getTitle() }}</a></li>
    @if (isset($object->parent))
        <li class="breadcrumb-item"><a href="{{ $object->parent->link }}">{{ $object->parent->title }}</a></li>
    @elseif (isset($object->category))
        <li class="breadcrumb-item"><a href="{{ $object->category->link }}">{{ $object->category->title }}</a></li>
    @endif
    <li class="breadcrumb-item active" aria-current="page"><a href="{{ url()->current() }}}" aria-current="page">{{ $object->title }}</a></li>
  </ol>
</nav>
