<div class="field">
    <label class="label">{{ __('admin.fields.' . $resource . '.' . $attribute) }}</label>
    <div class="control">
        @php
            $value = (${$resource}->$attribute ?? old($attribute));
            if($value) {
                $value = explode('storage/news/images/',$value)[1];
            }

        @endphp
        @if(!isset( $value))
            <input type="hidden" name="required_image" value="true" >
        @endif
        <div class="center-file file has-name is-boxed">
            <label id="file-box" class="file-label">
                <input id="file" class="file-input" type="file" name="image" accept="image/*" >
                <span class="file-cta">
                    <span class="file-icon">
                        {!! icon('upload') !!}
                    </span>
                    <span class="file-label">
                        {{ __('admin.fields.' . $resource . '.choose') }}
                    </span>
                </span>
                @if(isset($value))
                    <span class="file-name">{{$value}}</span>
                @endif
            </label>
        </div>
    </div>
</div>
