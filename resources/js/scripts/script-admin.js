$(document).ready(() => {
	$('#file').change((e) => {
		var fileName = e.target.files[0].name;
		if ($('#file-box').find('span.file-name').length !== 0) {
			$('#file-box').find('span.file-name').remove();
		}
		$('#file-box').append(`<span class="file-name">${fileName}</span>`);
	});
});

function submitBtn(form_delete, id_delete, resource) {
	const data = $(`#${form_delete}`).serializeArray();
	$.ajaxSetup({
		headers : {
			'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
		}
	});

	$.ajax({
		url     : `/admin/${resource}/${id_delete}`,
		data,
		type    : 'post',
		success : function(response) {
			location.reload(true);
		},
		error   : function(x, xs, xt) {
			if (x.responseJSON.hasOwnProperty('message')) {
				alert(x.responseJSON.message);
			}
		}
	});
}
