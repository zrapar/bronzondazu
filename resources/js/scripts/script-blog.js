(function($) {
	'use strict';

	$(window).on('load', function() {
		// Site loader
		$('.loader-inner').fadeOut();
		$('.loader').delay(200).fadeOut('slow');
		aos_init();
	});

	var header = $('.header'),
		pos = header.offset(),
		blockTop = $('.block-top');

	$(window).scroll(function() {
		if ($(this).scrollTop() > pos.top + 90 && header.hasClass('default')) {
			header.fadeOut('fast', function() {
				$(this).removeClass('default').addClass('switched-header').fadeIn(200);
				blockTop.addClass('active');
			});
		} else if ($(this).scrollTop() <= pos.top + 90 && header.hasClass('switched-header')) {
			header.fadeOut('fast', function() {
				$(this).removeClass('switched-header').addClass('default').fadeIn(100);
				blockTop.removeClass('active');
			});
		}
	});

	const heightHeader = parseInt($('.header.default').outerHeight());

	$('#blog-content').css('margin-top', `${heightHeader}px`);

	$('#articles').css('margin-top', `${heightHeader}px`);

	const heroImage = $('.hero-image');
	const dataHeroImage = heroImage.data();
	heroImage.css(
		'background-image',
		`linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url("${dataHeroImage.img}")`
	);

	var mobileBtn = $('.mobile-but');
	var nav = $('.main-nav ul');
	var navHeight = nav.height();

	$(mobileBtn).on('click', function() {
		$('.toggle-mobile-but').toggleClass('active');
		nav.slideToggle();
		$('.main-nav li a').addClass('mobile');
		return false;
	});

	$(window).resize(function() {
		var w = $(window).width();
		if (w > 320 && nav.is(':hidden')) {
			nav.removeAttr('style');
			$('.main-nav li a').removeClass('mobile');
		}
	});

	$('.main-nav li a').on('click', function() {
		if ($(this).hasClass('mobile')) {
			nav.slideToggle();
			$('.toggle-mobile-but').toggleClass('active');
		}
	});

	// Initi AOS
	function aos_init() {
		AOS.init({
			duration : 1000,
			once     : true
		});
	}
})(jQuery);
