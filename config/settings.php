<?php

return [
    'analytics_id'     => null,
    'logo'             => 'i/icons/android-chrome-192x192.png',
    'login_image'      => 'https://ssl.gstatic.com/accounts/ui/avatar_2x.png',
    'site_description' => 'BRONZON & DAZU',
    'site_title'       => 'BRONZON & DAZU'
];