<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', ['as' => 'root', 'uses' => 'PageController@getIndex']);
Route::get('article/{articleSlug}', ['as' => 'article', 'uses' => 'PageController@getArticle']);
Route::get('news', ['as' => 'news', 'uses' => 'PageController@getAllArticlesCategory']);
// Route::get('page/{pSlug}', ['as' => 'page', 'uses' => 'PageController@getPage']);
Route::get('category/{categorySlug}', ['as' => 'category', 'uses' => 'PageController@getCategory']);
Route::get('sitemap.xml', ['as' => 'sitemap', 'uses' => 'PageController@getSitemap']);