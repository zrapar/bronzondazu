const mix = require('laravel-mix'),
	assetsDir = 'resources/',
	distDir = 'public/dist/';

mix
	.copyDirectory(assetsDir + 'scss/font', distDir + 'fonts')
	.sass(assetsDir + 'scss/admin.scss', distDir + 'css/admin.css')
	.sass(assetsDir + 'scss/app.scss', distDir + 'css/app.css')
	.options({
		processCssUrls : false
	})
	.js(assetsDir + 'js/admin.js', distDir + 'js/admin.js')
	.js(assetsDir + 'js/app.js', distDir + 'js/app.js')
	.scripts(assetsDir + 'js/scripts/script-admin.js', distDir + 'js/script-admin.js')
	.scripts(assetsDir + 'js/scripts/audio.min.js', distDir + 'js/audio.min.js')
	.scripts(assetsDir + 'js/scripts/jquery.countdown.min.js', distDir + 'js/jquery.countdown.min.js')
	.scripts(assetsDir + 'js/scripts/jquery.flexslider-min.js', distDir + 'js/jquery.flexslider-min.js')
	.scripts(assetsDir + 'js/scripts/jquery.magnific-popup.min.js', distDir + 'js/jquery.magnific-popup.min.js')
	.scripts(assetsDir + 'js/scripts/placeholders.min.js', distDir + 'js/placeholders.min.js')
	.scripts(assetsDir + 'js/scripts/script.js', distDir + 'js/script.js')
	.scripts(assetsDir + 'js/scripts/script-blog.js', distDir + 'js/script-blog.js')
	.scripts(assetsDir + 'js/scripts/smooth-scroll.js', distDir + 'js/smooth-scroll.js')
	.scripts(assetsDir + 'js/scripts/aos.js', distDir + 'js/aos.js')
	.scripts(assetsDir + 'js/scripts/twitterFetcher_min.js', distDir + 'js/twitterFetcher_min.js');

if (mix.inProduction()) {
	mix.version();
}
