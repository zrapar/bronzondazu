<?php

namespace App\Http\Controllers\Application;

use App\Base\Services\SitemapService;
use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Category;
use App\Models\Page;
use File;
use Storage;

class PageController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $lastPublished = Article::published()->take(2)->get();

        // $storage = Storage::disk('uploads');

        // $gallery = $storage->allFiles('gallery');

        // sort($gallery, SORT_NATURAL | SORT_FLAG_CASE);

        return view('app.landing', [
            'title' => getTitle(),
            'description' => getDescription(),
            'articles' => $lastPublished,
            // 'gallery' => $gallery,
            // 'url_gallery' => $storage->url('/')
        ]);
    }

    /**
     * @param \App\Models\Category $category
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCategory(Category $category)
    {
        return view('app.categories', [
            'title' => $category->title,
            'description' => $category->description,
            'articles' => Article::where('category_id', $category->id)->paginate(3),
            'categories' => Category::where('id', '!=', $category->id)->get(),
            'recent_posts' => Article::published()->take(4)->get()
        ]);
    }

    public function getAllArticlesCategory()
    {
        return view('app.categories', [
            'title' => 'Noticias',
            'description' => 'Todas las noticias',
            'articles' => Article::paginate(3),
            'categories' => Category::get(),
            'recent_posts' => Article::published()->take(4)->get()
        ]);
    }

    /**
     * @param \App\Models\Page $page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPage(Page $page)
    {
        return view('app.content', ['object' => $page]);
    }

    /**
     * @param \App\Models\Article $article
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getArticle(Article $article)
    {
        return view('app.content', ['object' => $article, 'categories' => Category::all(), 'recent_posts' => Article::published()->take(4)->get()]);
    }

    /**
     * @param \App\Base\Services\SitemapService $sitemapService
     *
     * @return mixed
     * @throws \Exception
     */
    public function getSitemap(SitemapService $sitemapService)
    {
        return $sitemapService->render();
    }
}