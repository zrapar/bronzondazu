<?php

namespace App\Http\Controllers\Admin;

use App\Base\Controllers\AdminController;
use App\Http\Controllers\Admin\DataTables\ArticleDataTable;
use App\Models\Category;
use App\Models\Article;
use Illuminate\Http\Request;
use Storage;
use Image;
use File;

class ArticleController extends AdminController
{
    /**
     * @var array
     */
    public function __construct()
    {
        $this->storage = Storage::disk('public');
    }

    protected $validation = [
        'content'      => 'required|string',
        'category_id'  => 'required|integer',
        'description'  => 'required|string|max:200',
        'published_at' => 'required|string',
        'title'        => 'required|string|max:200',
        'required_image' => 'string',
        'image' => 'required_if:required_image,true|string'
    ];

    /**
     * @param \App\Http\Controllers\Admin\DataTables\ArticleDataTable $dataTable
     *
     * @return mixed
     */
    public function index(ArticleDataTable $dataTable)
    {
        return $dataTable->render('admin.table', ['link' => route('admin.article.create')]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function create()
    {
        return view('admin.forms.article', $this->formVariables('article', null, $this->options()));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $data = $request->all();

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $image_name = $image->getClientOriginalName();
            $images = $this->saveImage($image, $image_name, NULL);
            $data['image'] = $images['image'];
            $data['thumbnail'] = $images['thumbnail'];
            unset($data['required_image']);
        }

        $newRequest = new Request($data);



        return $this->createFlashRedirect(Article::class, $newRequest);
    }

    /**
     * @param \App\Models\Article $article
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function show(Article $article)
    {
        return view('admin.show', ['object' => $article]);
    }

    /**
     * @param \App\Models\Article $article
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function edit(Article $article)
    {

        return view('admin.forms.article', $this->formVariables('article', $article, $this->options()));
    }

    /**
     * @param \App\Models\Article $article
     * @param \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function update(Article $article, Request $request)
    {
        if ($request->hasFile('image')) {
            $filePath = str_replace($this->storage->url('/'), '', $article->image);
            $fileThumbnailPath = str_replace($this->storage->url('/'), '', $article->thumbnail);

            if ($this->storage->exists($filePath)) {
                $this->storage->delete($filePath);
            }

            if ($this->storage->exists($fileThumbnailPath)) {
                $this->storage->delete($fileThumbnailPath);
            }

            $data = $request->all();
            $image = $request->file('image');
            $image_name = $image->getClientOriginalName();
            $images = $this->saveImage($image, $image_name, NULL);
            $data['image'] = $images['image'];
            $data['thumbnail'] = $images['thumbnail'];
            $newRequest = new Request($data);

            return $this->saveFlashRedirect($article, $newRequest);
        }

        return $this->saveFlashRedirect($article, $request);
    }

    /**
     * @param \App\Models\Article $article
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Article $article, Request $request)
    {
        $filePath = str_replace($this->storage->url('/'), '', $article->image);
        $fileThumbnailPath = str_replace($this->storage->url('/'), '', $article->thumbnail);

        if ($this->storage->exists($filePath)) {
            $this->storage->delete($filePath);
        }

        if ($this->storage->exists($fileThumbnailPath)) {
            $this->storage->delete($fileThumbnailPath);
        }

        return $this->destroyFlashRedirect($article, 'index', $request->ajax());
    }

    /**
     * @return array
     */
    protected function options()
    {
        return ['options' => Category::pluck('title', 'id')];
    }

    protected function saveImage($image, $image_name)
    {
        $data = [
            'image' => '',
            'thumbnail' => '',
        ];

        $folder = storage_path("app/public/news/images/{$image_name}");

        $folderThumbnail = storage_path("app/public/news/thumbnail/{$image_name}");

        if (!File::isDirectory(storage_path("app/public/news/images"))) {
            $this->storage->makeDirectory('news/images');
            $this->storage->makeDirectory('news/thumbnail');
        }

        $principalImage = Image::make($image)->interlace()->orientate()->save($folder, 80);

        $thumbnail = Image::make($image)->interlace()->orientate()->resize(128, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->save($folderThumbnail, 80);


        $url = $this->storage->url("news/images/{$image_name}");

        $urlThumbnail = $this->storage->url("news/thumbnail/{$image_name}");

        $data['image'] = $url;
        $data['thumbnail'] = $urlThumbnail;

        $principalImage->destroy();
        $thumbnail->destroy();



        return $data;
    }
}