<?php

namespace App\Console\Commands\Cms;

use Illuminate\Console\Command;

class CacheClear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms:cache-clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear cache';

    public function handle()
    {
        $this->call('route:cache');
        $this->call('view:cache');
        $this->call('cache:clear');
        $this->info('All Cache cleared');
    }
}